# ArduinoPusher

This is to push some code to Arduino boards from the browser

## Requirement

+ [nodejs.org](http://nodejs.org)
+ [RMeurisse / ArduinoBuilder](https://github.com/RMeurisse/ArduinoBuilder)
+ SSL/TSL for non local environment (you will have to make ArduinoBuilder serve over SSL)

## Install

+ `npm install`
+ `npm run dev`

## Caveats

The Web Serial API is currently in development and is only available behind a flag on the stable branch of Chrome. Please enable the `#enable-experimental-web-platform-features` flag in `chrome://flags` to run this example.
or use [origin trial](https://github.com/GoogleChrome/OriginTrials/blob/gh-pages/developer-guide.md)

Only tested on Ubuntu 20.04 with Chrome latest

## Thanks to

+ [noopkat / avrgirl-arduino](https://github.com/noopkat/avrgirl-arduino/)
+ [RMeurisse / ArduinoBuilder](https://github.com/RMeurisse/ArduinoBuilder)
+ [BlocklyDuino / BlocklyDuino-v2](https://github.com/BlocklyDuino/BlocklyDuino-v2)
