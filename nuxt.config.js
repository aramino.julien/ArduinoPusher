import path from 'path'
import fs from 'fs'
import colors from 'vuetify/es5/util/colors'
console.log(process.env.ENV)
export default {
  server: {
    https: process.env.ENV === 'dev' ? false : {
      key: fs.readFileSync(path.resolve(process.env.SSL_PATH, 'privkey.pem')),
      cert: fs.readFileSync(path.resolve(process.env.SSL_PATH, 'cert.pem'))
    },
    port: process.env.PORT, // par défaut : 3000
    host: process.env.HOST // par défaut : localhost
  },
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s - ArduinoPusher',
    title: 'ArduinoPusher',
    script: [
      // {
      //   src: '/blockly_compressed.js', body: true
      // },
      // {
      //   src: '/arduino_compressed.js', body: true, defer: true
      // }
    ],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { 'http-equiv': 'origin-trial', content: process.env.ORIGINTRIAL_SERIAL }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    // {
    //   src: '~/plugins/arduino.js',
    //   mode: 'client'
    // }
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxt/http',
    '@nuxtjs/dotenv',
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    babel: {
      compact: true
    }
  }
}
